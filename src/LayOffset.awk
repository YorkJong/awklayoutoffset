# @file LayOffset.awk
# This script adds offsets to coordinates in a YAML layout file (e.g.,
# layout.yaml).
#
# @author Jiang Yu-Kuan <yukuan.jiang@gmail.com>
# @date 2015/10/21 (initial version)
# @date 2016/01/12 (last revision)
# @version 1.0

function version() {
    print "Layout Offset, LayOffset v1.0"
    print "                Jiang Yu-Kuan <yukuan.jiang@gmail.com>"
    print "                2016/01/12"
}

function help() {
    print "Adds offsets to coordinates in a YAML layout file (e.g.,",
          "layout.yaml)"
    print
    print "Usage: LayOffset [OPTIONS] InputFile > OutputFile"
    print "       LayOffset -v cmd=help"
    print "       LayOffset -v cmd=version"
    print "Options:"
    print "    -v OrgX=N       assign a horizontal offset"
    print "    -v OrgY=N       assign a vertical offset"
    print "    -v cmd=help     show this help message and exit"
    print "    -v cmd=version  show version message and exit"
}


BEGIN {
    if (cmd == "version") {
        version()
        exit
    }
    if (cmd == "help") {
        help()
        exit
    }

    OrgX += 0
    OrgY += 0
}

match($0, /(.*coord:.*\[)([0-9]+)(,[ \t]+)([0-9]+)(.*)/, m) {
    m[2] += OrgX
    m[4] += OrgY
    for (i=1; i<=5; ++i)
        printf "%s", m[i]
    printf "\n"
}

!/coord:/ #{print}

END {}
