:: @file LayOffset.bat
:: This batch file wraps LayOffset.awk to add offsets to coordinates in
:: a YAML layout file (e.g., layout.yaml).
::
:: @author Jiang Yu-Kuan <york_jiang@mars-semi.com.tw>
:: @date 2015/10/21 (initial version)
:: @date 2015/11/30 (last revision)
:: @version 1.0
::
:: Usage
:: -----
:: After dragging a YAML layout file (e.g., layout.yaml) to the batch file,
:: a console window will prompt you to enter a horizontal offset and enter a
:: vertical offset. After that, The console window will display another layout
:: file after coordinates offseting.

@echo off
set LayOffset=LayOffset.exe

set bat_dir=%~dp0
cd /D %bat_dir%

echo ^> Help
%LayOffset% -v cmd=help

echo:
echo:
echo ^> Parameters
set /p "x=Enter horizontal offset: "
set /p "y=Enter vertical offset: "

set infile=%1
if "%infile%" == "" (
    set infile=layout.yaml
)

echo:
echo ^> Converting
%LayOffset% -v OrgX=%x% -v OrgY=%y% %infile%


pause
