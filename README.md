# LayoutOffset #

LayoutOffset is an open source AWK application to add offsets to coordinates
in a YAML layout file (e.g., *layout.yaml*).

A YAML layout file describes multiple picture layouts of screens. I invented
the syntax under the standard YAML and used to assemble separated sub-pictures
into whole-pictures.

For the ease of use on Windows, I compiled the AWK script into an EXE manner
and wrapped it with a batch file.


## Install ##

1. Download a binary distribution (e.g., *LayoutOffset-1.0-bin.zip*) from
   [Downloads](https://bitbucket.org/YorkJong/awklayoutoffset/downloads) page.
2. Uncompress the binary distribution.


## Getting Started ##

1. Install LayoutOffset.
2. Drag a YAML layout file (e.g., *layout.yaml*) to the batch file
   *LayOffset.bat*.
3. A console window will prompt you to enter a horizontal offset,
   and enter a vertical offset.
4. After key in the offsets, the console window will list the layout
   that has offseted coordinates.


## An Example ##

Let's say that I had a YAML layout file **layout.yaml** with the content as
follows:
```yaml
---
name: CoordDemo
layout: {kind: coordinate}
align: left
parts:
  - {image: g0_baozi.png, coord: [43, 56]}
  - {image: g0_mahuan.png, coord: [254, 56]}
  - {image: g0_spring-roll.png, coord: [468, 56]}
  - {text: "Baozi", coord: [43, 184]}
  - {text: "Mahuan", coord: [254, 184]}
  - {text: "Spring Roll", coord: [468, 184]}
...
```

I dragged the *layout.yaml* to *LayOffset.bat* and gave parameters -43,
-56 as below:
```
> Parameters
Enter horizontal offset: -43
Enter vertical offset: -56
```

After converting, I got the result YAML layout.
```yaml
---
name: CoordDemo
layout: {kind: coordinate}
align: left
parts:
  - {image: g0_baozi.png, coord: [0, 0]}
  - {image: g0_mahuan.png, coord: [211, 0]}
  - {image: g0_spring-roll.png, coord: [425, 0]}
  - {text: "Baozi", coord: [0, 128]}
  - {text: "Mahuan", coord: [211, 128]}
  - {text: "Spring Roll", coord: [425, 128]}
...
```

## Command Line ##

```
Usage: LayOffset.exe [OPTIONS] InputFile > OutputFile
       LayOffset.exe -v cmd=help
       LayOffset.exe -v cmd=version
Options:
    -v OrgX=N       assign a horizontal offset
    -v OrgY=N       assign a vertical offset
    -v cmd=help     show this help message and exit
    -v cmd=version  show version message and exit
```
